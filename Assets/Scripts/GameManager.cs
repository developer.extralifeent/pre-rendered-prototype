﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[System.Serializable]
public class Rooms
{
    public Transform map;
    public GameObject vCam;
}

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField] private Image blackout;
    public Transform currentMap;
    [SerializeField] List<Rooms> rooms = new List<Rooms>();

    private void Awake()
    {
        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ChangeMap(int roomIndex)
    {
        foreach(Rooms r in rooms)
        {
            r.vCam.SetActive(false);
            r.map.transform.parent.gameObject.SetActive(false);
        }

        rooms[roomIndex].map.transform.parent.gameObject.SetActive(true);
        rooms[roomIndex].vCam.SetActive(true);
        currentMap = rooms[roomIndex].map;
        PlayerMovement.instance.ChangeMap();
    }

    public void FadeOut(bool fade)
    {
        float value = fade ? 1 : 0;
        blackout.DOColor(new Color(0, 0, 0, value), 1f).OnComplete(() => {
            PlayerMovement.instance.CanMove(!fade);
        });
    }
}
