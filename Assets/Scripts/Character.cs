﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    private float originalScaleFactor;
    public float OriginalScaleFactor { get => originalScaleFactor; }

    protected void Start()
    {
        originalScaleFactor = transform.localScale.x;
    }
}
