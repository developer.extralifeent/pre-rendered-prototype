﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpeedChanger : MonoBehaviour
{
    private float normalMoveSpeed;
    [SerializeField] private float moveSpeed;
    [SerializeField] private bool affectAnimation = true;
    private static int triggerEntered = 0;


    public void Start()
    {
        normalMoveSpeed = PlayerMovement.instance.MoveSpeed;
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            triggerEntered++;
            PlayerMovement.instance.SetMoveSpeed(moveSpeed);
            PlayerMovement.instance.SetAnimateWalkSpeed(affectAnimation);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            triggerEntered--;
            if(triggerEntered <= 0)
            {
                triggerEntered = 0;
                PlayerMovement.instance.SetMoveSpeed(normalMoveSpeed);
                PlayerMovement.instance.SetAnimateWalkSpeed(true);
            }
            
        }
    }
}
