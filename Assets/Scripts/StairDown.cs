﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairDown : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] float maxHeight;
    [SerializeField] float minHeight;
    [SerializeField] float sideMoveSpeed = 0.01f;
    [SerializeField] float moveSpeed;
    [SerializeField] GameObject invisibleMask;
    [SerializeField] GameObject upEnabler;

    private void OnTriggerStay(Collider other)
    {
        if(Input.GetAxis("Vertical") > 0.1f && transform.localPosition.z < minHeight)
        {
            PlayerMovement.instance.CanWalk(false);
            other.transform.position += Vector3.down * moveSpeed * Time.deltaTime;
            transform.position += Vector3.down * moveSpeed * Time.deltaTime;
            other.transform.position += Vector3.right * sideMoveSpeed;
        }
        else if (Input.GetAxis("Vertical") < -0.1f && transform.localPosition.z > maxHeight)
        {
            PlayerMovement.instance.CanWalk(false);
            other.transform.position += Vector3.up * moveSpeed * Time.deltaTime;
            transform.position += Vector3.up * moveSpeed * Time.deltaTime;
            other.transform.position += Vector3.left * sideMoveSpeed;
        }

        if(transform.localPosition.z <= maxHeight || transform.localPosition.z >= minHeight)
        {
            PlayerMovement.instance.CanWalk(true);
            StartCoroutine(EnableSelf());
        }

        //kalo diatas
        if (transform.localPosition.z <= maxHeight)
        {
            invisibleMask.SetActive(false);
            upEnabler.SetActive(true);
        }
        //kalo dibawah
        else if (transform.localPosition.z >= minHeight)
        {
            upEnabler.SetActive(false);
        }

    }

    private void OnTriggerExit(Collider other)
    {
        PlayerMovement.instance.CanWalk(true);
        gameObject.SetActive(true);
    }

    public void MoveUp()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, maxHeight);
    }

    public void MoveDown()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, minHeight);
    }

    IEnumerator EnableSelf()
    {
        gameObject.SetActive(false);
        yield return new WaitForFixedUpdate();
        gameObject.SetActive(true);
        Debug.Log("Enable");
    }
}
