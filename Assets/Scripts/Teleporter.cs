﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Teleporter : MonoBehaviour
{
    [System.Serializable] public class OnTeleport : UnityEvent { };
    [SerializeField] public OnTeleport onTeleport;

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            StartCoroutine(TeleportCoroutine());
    }

    public void TeleportPlayer(Transform newPosition)
    {
        PlayerMovement.instance.transform.position = newPosition.position;
    }

    IEnumerator TeleportCoroutine()
    {
        PlayerMovement.instance.CanMove(false);
        GameManager.instance.FadeOut(true);
        yield return new WaitForSecondsRealtime(1f);
        onTeleport.Invoke();
        GameManager.instance.FadeOut(false);
    }
}
