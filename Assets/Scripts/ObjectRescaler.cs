﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRescaler : MonoBehaviour
{

    // Start is called before the first frame update
    [SerializeField] float minimumScale = 0.1f;

    private void OnTriggerStay(Collider other)
    {
        float nominator = other.ClosestPoint(transform.position).y;
        float denominator = (transform.position + transform.up * (transform.localScale.z / 2)).y;
        float newScale = Mathf.Abs(nominator - denominator) / transform.localScale.z;

        newScale *= 3;

        if (newScale > 1) newScale = 1;
        if (newScale < minimumScale) newScale = minimumScale;

        float scaleFactor = 0.3f;
        if (other.GetComponent<Character>()) scaleFactor = other.GetComponent<Character>().OriginalScaleFactor;
        else if (other.GetComponent<PlayerMovement>()) scaleFactor = other.GetComponent<PlayerMovement>().OriginalScaleFactor;

        other.transform.localScale = scaleFactor * newScale * Vector3.one;
    }
}
