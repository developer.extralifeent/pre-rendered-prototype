﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputRotator : MonoBehaviour
{
    // Start is called before the first frame update
    public enum MoveType
    {
        LeftRight,
        UpDown,
        None
    }

    [SerializeField] private MoveType affectedMove;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerMovement.instance.SetStairsAngle(transform.localRotation.eulerAngles.y);
            PlayerMovement.instance.SetMoveType(affectedMove);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerMovement.instance.SetStairsAngle(0);
            PlayerMovement.instance.SetMoveType(MoveType.None);
        }
    }
}
