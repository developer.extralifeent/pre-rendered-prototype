﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectEnabler : MonoBehaviour
{
    [System.Serializable] public class OnEnter : UnityEngine.Events.UnityEvent { };
    [SerializeField] public OnEnter onEnter;

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            onEnter.Invoke();
        }
    }
}
