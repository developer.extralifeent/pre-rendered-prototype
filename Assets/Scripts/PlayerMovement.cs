﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : Character
{
    public static PlayerMovement instance;
    protected void Awake()
    {
        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private const string HorizontalAxisKey = "Horizontal";
    private const string VerticalAxisKey = "Vertical";

    [SerializeField] private float moveSpeed;
    public float MoveSpeed { get => moveSpeed; } public void SetMoveSpeed(float newMoveSpeed) { moveSpeed = newMoveSpeed; }
    private float stairsAngle;  public void SetStairsAngle(float newAngle) { stairsAngle = newAngle; }
    private bool canMove; public void CanMove(bool move) { canMove = move; }  public bool CanMoveOrNot { get => canMove; }
    private bool canWalk; public void CanWalk(bool move) { canWalk = move; } public bool CanWalkOrNot { get => canWalk; }
    private InputRotator.MoveType moveType; public void SetMoveType(InputRotator.MoveType newMoveType) { moveType = newMoveType; }

    private bool animateWalk; public void SetAnimateWalkSpeed(bool set) { animateWalk = set; }
    private float originalMoveSpeed;

    private float elevationAngle;
    private Rigidbody rbody;

    private Animator anim;
    private void Start()
    {
        base.Start();
        canMove = canWalk = true;
        stairsAngle = 0;
        originalMoveSpeed = moveSpeed;
        moveType = InputRotator.MoveType.None;
        rbody = GetComponent<Rigidbody>();
        anim = transform.GetChild(0).gameObject.GetComponent<Animator>();
        ChangeMap();
    }

    public void ChangeMap()
    {
        Transform map = GameManager.instance.currentMap;
        elevationAngle = map.transform.rotation.eulerAngles.x;
        transform.GetChild(1).transform.rotation = Quaternion.Euler(new Vector3(-elevationAngle, 0, 0));
    }

    private void FixedUpdate()
    {
        Vector3 input;
        input = canMove ?  new Vector3(Input.GetAxis(HorizontalAxisKey), 0, Input.GetAxis(VerticalAxisKey)) : Vector3.zero;

        float moveValue = Mathf.Max(Input.GetAxis(HorizontalAxisKey), Input.GetAxis(VerticalAxisKey));
        //rotate input for side stairs
        if ((Mathf.Abs(Input.GetAxis(HorizontalAxisKey)) > 0.1f && moveType == InputRotator.MoveType.LeftRight))
        {
            if ((Mathf.Abs(Input.GetAxis(HorizontalAxisKey)) > 0.1f)) input = new Vector3(Input.GetAxis(HorizontalAxisKey), 0, 0);
            if ((Mathf.Abs(Input.GetAxis(VerticalAxisKey)) > 0.1f)) input = new Vector3(-Input.GetAxis(VerticalAxisKey), 0, 0);
            input = Quaternion.Euler(new Vector3(0, -stairsAngle, 0)) * input;
        }
        else if ((Mathf.Abs(Input.GetAxis(VerticalAxisKey)) > 0.1f && moveType == InputRotator.MoveType.UpDown))
        {
            if ((Mathf.Abs(Input.GetAxis(HorizontalAxisKey)) > 0.1f)) input = new Vector3(0, 0, -Input.GetAxis(HorizontalAxisKey));
            if ((Mathf.Abs(Input.GetAxis(VerticalAxisKey)) > 0.1f)) input = new Vector3(0, 0, Input.GetAxis(VerticalAxisKey));
            input = Quaternion.Euler(new Vector3(0, -stairsAngle, 0)) * input;
        }

        if (input.magnitude != 0) 
            transform.GetChild(0).transform.rotation = Quaternion.LookRotation(Quaternion.Euler(new Vector3(0, -90, 0)) * input, Vector3.up);

        //rotate input for elevation angle
        input = Quaternion.Euler(new Vector3(-elevationAngle, 0, 0 )) * input;

        //movement
        if(canWalk) rbody.AddForce(input * moveSpeed, ForceMode.Impulse);

        //sticking force
        rbody.AddForce(Quaternion.Euler(new Vector3(-elevationAngle, 0, 0)) * Vector3.down * 10f, ForceMode.Acceleration);

        anim.SetFloat("moveSpeed", rbody.velocity.magnitude);

        if(animateWalk)
            anim.SetFloat("walkSpeed", rbody.velocity.magnitude * 2f);
        else if(!canWalk)
            anim.SetFloat("walkSpeed", 1.5f);
        else
            anim.SetFloat("walkSpeed", rbody.velocity.magnitude * originalMoveSpeed * 2f / moveSpeed);
    }
}
